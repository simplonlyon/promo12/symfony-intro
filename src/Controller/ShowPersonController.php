<?php

namespace App\Controller;

use App\Repository\PersonRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ShowPersonController extends AbstractController {

    /**
     * une route vide ou avec juste "/" sera accessible directement
     * sur la racine du projet (http://localhost:8000)
     * @Route("", name="home")
     */
    public function index(PersonRepository $repo) { //On injecte notre DAO
        //On l'utilise pour faire un findAll pour aller chercher toutes
        //les personnes présentent en bdd
        $persons = $repo->findAll();
        //On donne les personnes au template twig
        return $this->render('show-person.html.twig', [
            'persons' => $persons
        ]);
    }

    /**
     * On peut faire des routes avec des paramètres à l'intérieur,
     * ces paramètres devront être entourés d'accolades
     * @Route("/person/{id}", name="one_person")
     */
    public function onePerson(int $id) { //On peut ensuite ajouter le paramètre en argument de la méthode en mettant le même nom que dans les accolades de la route
        $repo = new PersonRepository();
        $person = $repo->findById($id);
        dump($person);
        return $this->render('one-person.html.twig', [
            'person' => $person
        ]);
    }

}