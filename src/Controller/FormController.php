<?php

namespace App\Controller;

use App\Entity\Person;
use App\Form\PersonType;
use App\Repository\PersonRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class FormController extends AbstractController
{
    private $list = ["item 1", "item 2", "item 3"];

    /**
     * @Route("/form", name="first_form")
     */
    public function firstForm(Request $request) // La Request est une classe de symfony qui contiendra toutes les informations et paramètres de la requête http
    {
        $inputCtrl = $request->get("input-tag");
        if ($inputCtrl) {
            $this->list[] = $inputCtrl;
            // array_push($this->list, $inputCtrl);
        }
        return $this->render("form.html.twig", [
            "inputView" => $inputCtrl,
            "list" => $this->list
        ]);
    }

    /**
     * @Route("/form-person", name="form_person")
     */
    public function formPerson(Request $request)
    {
        //On crée une variable personne qui de base sera null
        $person = null;
        //On récupère les valeurs des différents input en utilisant la request
        $name = $request->get("name");
        $age = $request->get("age");
        $personality = $request->get("personality");
        //On vérifie que chaque input contenait bien une valeur
        if ($age && $name && $personality) {
            //Si c'est le cas, on utilise ces valeurs pour créer une 
            //nouvelle instance de Person qu'on met dans la variable
            $person = new Person($name, $personality, $age);
            //Qu'on affiche dans la console avec un dump
            
            $repo = new PersonRepository();
            $repo->add($person);
        }
        //On fait un render du template en lui donnant à manger la
        //variable person qui contient soit une instance soit rien
        return $this->render("form-person.html.twig", [
            'person' => $person
        ]);
    }

    /**
     * @Route("/form-symfony", name="form_symfony")
     */
    public function formSymfony(Request $request) {
        //On crée une instance de notre formulaire en lui indiquant entre
        //les parenthèses quelle formulaire on souhaite générer
        $form = $this->createForm(PersonType::class);
        //On donne la request à manger au formulaire pour qu'il traite
        //les données contenue dans celle ci
        $form->handleRequest($request);
        //On indique ce que l'on fera si le formulaire a été soumis
        //et si celui ci est valide
        if($form->isSubmitted() && $form->isValid()) {
            //On récupère les données du formulaire (ici, une instance
            //de Person)
            dump($form->getData());
        }

        return $this->render('form-symfony.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
