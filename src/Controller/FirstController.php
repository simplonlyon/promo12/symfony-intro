<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class FirstController extends AbstractController {
    
    public function page() {
        $name = 'Jean';
        return $this->render('first.html.twig', [
            'name' => $name
        ]);
    }
    /**
     * @Route("/annotation", name="annotation")
     */
    public function pageAnnotation() {
        return new Response('<p>Route with annotation</p>');
    }
}