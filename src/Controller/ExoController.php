<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


class ExoController extends AbstractController
{

    /**
     * @Route("/exo-twig", name="exo_twig")
     */
    public function exoTwig()
    {
        $number = rand(1, 100);
        $tab = ['ga','zo','bu','meu'];

        return $this->render('exo-twig.html.twig', [
            'randNumber' => $number,
            'tab' => $tab
        ]);
    }
}
