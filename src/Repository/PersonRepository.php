<?php

namespace App\Repository;

use App\Entity\Person;
/**
 * Un Repository, ou DAO (terme plus "pro", Data Access Object) est une
 * class dont le but est de concentré les appels à la base de données,
 * ainsi, le reste de l'application dépendra des DAO et on aura pas 
 * des appels bdd disseminés partout dans l'appli (comme ça, si jamais 
 * la table change, on le sgbd ou autre, on aura juste le DAO à 
 * modifier et pas tous les endroits où on aurait fait ces appels)
 */
class PersonRepository
{
    private $pdo;

    public function __construct() {
        /**
         * On fait une instance de PDO dans le constructeur
         * car on utilisera cette connexion dans toutes les
         * méthodes de cette classe (techniquement, il faudrait
         * même la mettre dans une classe à part dédiée à la
         * création de la connexion)
         * 
         * L'instance de PDO représente une connexion à la base de
         * données. Elle attend en argument le serveur sur laquelle
         * se trouve notre bdd (localhost), le nom de la bdd à laquelle
         * on veut se connecter (introduction), puis le username et le
         * password pour se connecter à cette base de données. (il 
         * serait préférable de mettre ces informations dans un fichier
         * de configuration hors du php)
         */
        $this->pdo = new \PDO(
            'mysql:host='.$_ENV['DATABASE_HOST'].';dbname=' . $_ENV['DATABASE_NAME'],
            $_ENV['DATABASE_USERNAME'],
            $_ENV['DATABASE_PASSWORD']
        );
    }

    /**
     * Méthode qui va aller chercher toutes les personnes
     * présentent dans la base de données et les convertir
     * en instance de la classe Person
     * @return Person[] les personnes contenues dans la bdd
     */
    public function findAll(): array
    {
        $query = $this->pdo->prepare('SELECT * FROM person');
    
        $query->execute();
        $results = $query->fetchAll();
        $list = [];
        foreach ($results as $line) {
            $person = $this->sqlToPerson($line);
            $list[] = $person;
        }
        return $list;
    }
    /**
     * Méthode permettant de faire persister une instance de Person
     * en base de données
     */
    public function add(Person $person): void {
        /**
         * On crée la requête pour faire une insertion, et dans cette
         * requête pour les valeurs à insérer dans la bdd, on va plutôt
         * mettre des placeholders (:name, :personality ...) qu'on
         * pourra assigner par la suite
         */
        $query = $this->pdo->prepare('INSERT INTO person (name,age,personality) VALUES (:name,:age,:personality)');
        //On assigne chaque valeur à faire persister venant de l'instance
        //de la classe Person aux placeholders définis dans la requête.
        //On peut également précisé en troisième argument le type SQL attendu
        $query->bindValue(':name', $person->getName(), \PDO::PARAM_STR);
        $query->bindValue(':age', $person->getAge(), \PDO::PARAM_INT);
        $query->bindValue(':personality', $person->getPersonality(), \PDO::PARAM_STR);
        //On exécute la requête
        $query->execute();
        //On assigne l'id généré par SQL à l'instance de la Person
        $person->setId(intval($this->pdo->lastInsertId()));
    }

    /**
     * Méthode permettant de récupérer une personne spécifique en utilisant
     * son id. Si la personne n'existe pas, on renvoie null
     */
    public function findById(int $id): ?Person {
        //On fait la requête SELECT mais avec un WHERE pour l'id cette fois
        $query = $this->pdo->prepare('SELECT * FROM person WHERE id=:idPlaceholder');
        //On assigne au placeholder la valeur contenu dans l'argument
        //id de la méthode
        $query->bindValue(':idPlaceholder', $id, \PDO::PARAM_INT);
        //On exécute la requête
        $query->execute();
        //On récupère le premier résultat de la requête
        $line = $query->fetch();
        //Si ce résultat existe bien
        if($line) {
            //Alors on renvoie l'instance de Person correspondante
            return $this->sqlToPerson($line);
            //return new Person($line['name'], $line['personality'], $line['age'], $line['id']);
        }
        //Sinon on renvoie null pour indiquer qu'aucune personne ne
        //correspondait à l'id fourni
        return null;

    }
    /**
     * Methode dont le but est de transformer une ligne de résultat
     * PDO en instance de la classe Person
     * Cette méthode est juste là dans un but de refactorisation afin
     * d'éviter la répétition dans les différents find
     */
    private function sqlToPerson(array $line):Person {
        return new Person($line['name'], $line['personality'], $line['age'], $line['id']);
    }
}
