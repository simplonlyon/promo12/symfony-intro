# Symfony Introduction

Projet symfony fait avec le symfony/skeleton afin de voir les différents modules au fur et à mesure

## How To Use
1. Cloner le projet
2. Faire un `composer install` dans le dossier
3. Créer à la racine un fichier .env.local qui devra contenir 4 variable : `DATABASE_HOST` `DATABASE_NAME` `DATABASE_PASSWORD` `DATABASE_USERNAME` avec comme valeur les informations de connexion de votre bdd
4. (si pas déjà fait) Importer la base de données `sudo mysql < bd.sql`
5. Faire un `bin/console server:run`
6. Accéder au projet sur http://localhost:8000

## Exercices

1. Dans le dossier Controller, créer un nouveau fichier/classe ExoController
2. Dans ce fichier, créer une méthode et une route sur l'url /exo-twig en utilisant la manière que vous préférez (routes.yaml OU annotation)
3. Dans le dossier templates, créer un fichier exo-twig.html.twig
4. Faire le render et tout dans la méthode du controller pour afficher votre template
5. Dans le controller, faire en sorte de récupérer un nombre au hasard entre 1 et 100 en utilisant la fonction rand() de php
6. Mettre ce nombre dans une variable et l'exposer au template twig
7. Dans le template, faire en sorte d'afficher le nombre
8. Dans le template toujours, rajouter un if (aidez vous de l'autocompletion pour voir comment il s'écrit) qui affichera "wow, big number" si jamais le nombre est supérieur à 20

### La boucle
1. Dans la même route que l'exo précédent, faire en sorte d'exposer un tableau avec 4-5 chaînes de caractères dedans (genre ga, zo, bu, meu)
2. Côté twig, utiliser une boucle for (aidez vous de l'autocomplete) pour faire en sorte d'afficher les item du tableau sous forme de <li> dans un <ul>

### I. Form Part One
1. Créer un nouveau contrôleur avec une route/méthode à l'intérieur sur l'url "/form"
2. Créer un fichier twig et le lié à la route que vous venez de faire
3. Dans ce fichier twig, créer un formulaire HTML qui contiendra un input type text avec un name, et un button
4. Dans les arguments de la méthode de votre route, rajouter un argument qui sera typé en Request (en faisant le use de symfony) avec le nom que vous voulez
5. A l'intérieur de la méthode, utiliser l'argument Request et déclencher sa méthode get() en lui donnant comme argument le name de l'input de votre formulaire, et stocker le retour de cette méthode dans une variable
6. Exposer cette variable au template et faire en sorte de l'afficher quelque part dans le template
### II. Liste de truc
1. Dans le contrôleur, créer une propriété list et initialiser la comme tableau avec 2-3 string dedans
2. Dans la route, faire en sorte d'exposer la propriété list au template
3. Dans le template, faire une boucle pour afficher toutes les valeurs de la list sous la forme que vous voulez
4. Dans la route, faire en sorte de pusher la valeur de l'input dans la propriété list
### III. Créer une Person via un formulaire
1. Récupérer la classe Person et la mettre dans le src (si ce n'est déjà fait)
2. Créer une nouvelle route dans le même contrôleur FormController et faire qu'elle pointe sur /form-person
3. Dans le twig de la nouvelle route, faire un formulaire avec les inputs nécessaires à la création d'une personne (name, age, personality)
4. Dans la méthode de la route, faire en sorte de récupérer les différentes valeurs de la requête et s'en servir pour faie une instance de person qu'on exposera à notre twig
5. Dans le twig, faire en sorte d'afficher la person qu'on vient de créer
### IV. Le retour du DAO (faire persister la Person en sql)
1. Récupérer le PersonRepository (et son dossier Repository) du projet php-pdo et le copier/coller dans notre projet symfony-intro dans le dossier src
2. Dans la route formPerson, faire une instance du Repository
3. Déclencher la méthode add du repository pour faire persister l'instance de personne qu'on a fait hier à partir du formulaire
### V. Afficher les personnes
1. Créer un nouveau contrôleur ShowPersonController avec dedans une route sur "/"
2. Dans la méthode de la route utiliser le Repository pour récupérer toutes les persones et les exposer au template twig
3. Dans le template twig, itérer sur les person et les afficher au format que vous voulez